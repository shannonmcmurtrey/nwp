class AddHaveYouWorkedWithAnAllyToAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :assessments, :have_you_recently_worked_with_an_ally, :boolean
  end
end
