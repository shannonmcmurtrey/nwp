class RegularAttenderToAssessments < ActiveRecord::Migration[5.2]
  def change
  	add_column :assessments, :attended_at_least_two_meetings_in_last_six_months, :boolean
  end
end
