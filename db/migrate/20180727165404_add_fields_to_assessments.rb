class AddFieldsToAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :assessments, :ss_non_criminal, :float
    add_column :assessments, :ss_support_system, :float
    add_column :assessments, :ss_food, :float
    add_column :assessments, :ss_home_safety, :float
    add_column :assessments, :ss_community_involvement, :float
  end
end
