class AddHowManyAlliesToAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :assessments, :how_many_allies, :float
  end
end
