class ChangeRecentFieldTypesInAssessments < ActiveRecord::Migration[5.2]
  def change
  	change_column :assessments, :ss_non_criminal, :integer
  	change_column :assessments, :ss_support_system, :integer
    change_column :assessments, :ss_food, :integer
    change_column :assessments, :ss_home_safety, :integer
    change_column :assessments, :ss_community_involvement, :integer
  end
end
